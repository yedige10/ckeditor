CKEDITOR.plugins.add( 'timestamp', {
    requires:'widget',
    icons: 'timestamp',

    
    init: function( editor ) {
        CKEDITOR.dialog.add( 'dialogForName', this.path + 'dialog/dialog.js' );
        
        
        editor.widgets.add( 'timestamp', {
          
            button: 'Create a simple box',

            template: '<input type="text" required />',
            editables: {},

            dialog: 'dialogForName',

            upcast: function( element ) {
                return element.name == 'div' && element.hasClass( 'simplebox' );
            },

            init: function() {

            },

            data: function() {
                if ( this.data.align )
                    this.element.addClass( this.data.align );
            }
        } );
    }
} );