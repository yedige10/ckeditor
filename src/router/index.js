import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

import EditorRouter from '@/views/Editor/MainRouter'
import EditorMain from '@/views/Editor/Main'
import EditorQuestionAdding from '@/views/Editor/EditorQuestionAdding'
import ToDo from '@/views/Editor/ToDo/app/ToDo'
import Calendar from '@/views/Editor/Calendar';
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    { 
      path: '/editor', 
      component: EditorRouter,
      children: [
        {
          path: '',
          name: 'EditorMain',
          component: EditorMain
        },
        {
          path: 'add-question',
          name: 'EditorQuestionAdding',
          component: EditorQuestionAdding
        },
        {
          path: 'todo',
          name: 'ToDo',
          component: ToDo
        },
        {
          path:'calendar',
          name:'Calendar',
          component:Calendar
        }
      ]
    }
  ]
})